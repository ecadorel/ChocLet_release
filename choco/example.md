# Example

# SEND + MORE = MONEY

```rust
import std.choco.globals;

let S = 1..9;
let E = 0..9;
let N = 0..9;
let D = 0..9;
let M = 1..9;
let O = 0..9;
let R = 0..9;
let Y = 0..9;

(1000 * S + 100 * E + 10 * N + 1 * D
 + 1000 * M + 100 * O + 10 * R + 1 * E
 == 10000 * M + 1000 * O + 100 * N + 10 * 1 * E + 1 * Y).post ();

allDifferent ([S,E,N,D,M,O,R,Y]).post ();
choco.solve ();

println ("    ", S, E, N, D);
println (" +  ", M, O, R, E);
println (" = ", M, O, N, E, Y);

```

## Execution 
```
    9567
 +  1085
 = 10652
```

# N QUEENS

```rust
import std.choco.globals;

let n = 4;

let vars = [1 .. n | i in 0 .. n];

let diagL = [vars [i] - i | i in 0 .. n];
let diagR = [vars [i] + i | i in 0 .. n];


allDifferent (vars).post ();
allDifferent (diagL).post ();
allDifferent (diagR).post ();

choco.solve ();
println (vars);
```

## Execution

```
[2, 4, 1, 3]
```

