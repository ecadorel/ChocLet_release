# Variables

ChocLet support basic Choco variables types.
- Integers
- Bool
- Real
- Set

## Integer vars - ChocoInt

The Choco IntVar, named ChocoInt in choclet language have a special binding syntax.

```rust
let a = 0 .. 10; // [0; 10(
// is the same as 

let a = choco.int (0, 9);

let b = 0 ... 10; // [0; 10]
// is the same as

let b = choco.int (0, 10);
```

The token `$`, can be used to define the maximum value that can be taken. With `$` token, only the operator `..` can be used.

```rust
let a = 0 .. $; // [0 ; 21474836]
let b = 0 ... $; // error, the maximum value cannot be contained in the set

let c = 0 .. ($ - 21474829); // [0 ; 7]
let d = -$ .. $; // [-21474836 ; 21474836]
```

### ChocoInt attributes

| Name | Value | Type |
|------|-------|------|
| `upper` | The upper value of the domain of the variable | int |
| `lower` | The lower value of the domain of the variable | int |
| `val` | The value of the variable if the var is instantiated | int |
| `isInstantiated` | True if the var has a value | bool |
| `abs` | The absolute value of the variable | ChocoInt |


### ChocoInt methods

- **remove** (*values*...) -> void |
  Remove a list of value inside the domain of the variable
  - Parameters : 
	- *values*, a set of `int`
  
- **preserve** (*values*...) -> void |
  Remove all values inside of the the domain of the variable except those in *values*
  - Parameters:
	- *values*, a set of `int`
- **maximize** () -> void |
  Use the variable as the problem objective 
- **minimize** () -> void |
  Use the variable as the problem objective 
- **max** (*other*) -> ChocoInt |
  Return a ChocoInt containing the constraint `max`
  - Parameters : 
	- *other*, a ChocoInt
- **min** (*other*) -> ChocoInt |
  Return a ChocoInt containing the constraint `min`
  - Parameters : 
	- *other*, a ChocoInt
