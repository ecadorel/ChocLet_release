# Choco

ChocLet was designed to be interoperable with Choco library.
The global identifier `choco`, allows to declare variables, define constraints and solve a model. 

## Choco binding

The `choco` is a binding of a Choco `Model` instance, where some function are overriden to ease the usage. 

```rust
let a = choco.int (0, 10);
// a is an intVar

println (a); //IV_1 = {0,1,2,3,4,5...,10}
```
### Choco Model direct binding


The following function are directly callable with `choco` object.
The prototype of the Java function are respected (direct binding).

| Tables   |      alias      |
|----------|-------------|
| `int`      |  [`intVar`](http://www.choco-solver.org/apidocs/org/chocosolver/solver/variables/IVariableFactory.html#intVar-int-) | 
| `real`     |  [`realVar`](http://www.choco-solver.org/apidocs/org/chocosolver/solver/variables/IVariableFactory.html#realVar-double-)     |
| `set` | [`setVar`](http://www.choco-solver.org/apidocs/org/chocosolver/solver/variables/IVariableFactory.html#setVar-int...-) |
| `bool` | [`boolVar`](http://www.choco-solver.org/apidocs/org/chocosolver/solver/variables/IVariableFactory.html#boolVar--) |
| `intArray` | [`intVarArray`](www.choco-solver.org/apidocs/org/chocosolver/solver/variables/IVariableFactory.html#intVarArray-int-int:A-) |
| `realArray` | [`realVarArray`](http://www.choco-solver.org/apidocs/org/chocosolver/solver/variables/IVariableFactory.html#realVarArray-int-double-double-double-) |
| `boolArray` | [`boolVarArray`](http://www.choco-solver.org/apidocs/org/chocosolver/solver/variables/IVariableFactory.html#boolVarArray-int-) |

### Choco custom binding

You can add your own binding to choco Model methods.

```rust
in choco {
	def allDifferents -> ChocoConstraint;
}

let a = [choco.int (0, 10), choco.int (1, 3)];
choco.allDifferents (a).post ();
```

### Choco model function override

The following function are provided by `choco` object.

- **post** (constraints...) -> void |
  This function will post the constraints to the model
  - Parameters : 
	- constraints, an set of [Constraints](choco/consts.md)
  
- **solve** () -> bool | 
  This function is a binding of the solve function, it will actually call the following Java code : 
  - Returns: is the model satisfied by current solution ?

  ```java
  Solver solver = model.getSolver ();
  return solver.solve ();
  ```
- **clean** () -> void |
  Clean the model, by removing all the posted variables and constraints
- **restart** () -> void |
  Restart the solving search, by calling the solver function [restart](http://www.choco-solver.org/apidocs/org/chocosolver/solver/Solver.html#restart--)
- **print** () -> void |
  Print the model to the screen
- **strategy** (varSelect, valSelect[, vars]) -> void |
  This function is used to monitor the solving execution
  Described in this section [Solving](choco/solving.md)
- **onOpenNode** (onOpen, onClose) -> void
  - Parameters :
	- onOpen : a function fn () -> void
	- onClose : a function fn () -> void
  
- **printStats** () -> void
  ```java
  model.getSolver ().printStatistics ();
  ```

- **solveOptimal** () -> bool | 
  Solve the problem until the optimal solution is found : 
  ```java
  Solution s = new Solution (model);
  while (model.getSolver ().solve ()) s.record ();
  boolean feasible = model.getSolver ().isFeasible () == ESat.TRUE;
  
  if (feasible) s.restore ();
  return feasible;
  ```
  
- **exportJSON** (filename) -> void | 
  Export the model to a json file 
  - Parameters: 
	- filename : the name of the file in which the exportation will be done
  
