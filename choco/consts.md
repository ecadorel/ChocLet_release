# Constraints 

## Arithmetic operation 

All the operators are overriden to ease the usage of Choco.

```rust
let a = 0 .. 10;
let b = 0 .. 20;
let c = a + b; // Ok, will call the java a.mul (b).intVar ();

let d = a + 3; 
let x = (d + c * 32) / b;
```


## Logical operation

As for arithmetic operation all the logical operator are overriden.
They return a BoolVar, or a ChocoBool (ChocLet name) that can be used
in another expression, or posted as a constraint to the model.

```rust
let a = 0 .. 10;
let b = 0 .. 10;
let c = a < b;
let d = a + b == 19;
(d && c).post ();
```

## Conditional constraints 

The operators `->` and `<->` allow to define simple or double
implications on expressions of type ChocoBool, or
ChocoConstraint. These specific constraints cannot be reified (used as
a value, and placed in a variable), so they are posted immediately.

```rust
let a = 0 .. 4;
let b = 0 .. 4;

(a == 2) -> (b == 1); // if a is equal to 2, then b is equal to 1

(a == 0) <-> (b == 0); // a is equal to 0 if and only if b is equal to 0

```

## Constraint binding

```rust
in choco {
    def allDifferent -> ChocoConstraint;
}

let a = [0 .. 10 | i in 0 .. 10];
choco.allDifferent (a).post ();

// Or using standard function
import std.choco.globals;

a.allDifferent ().post ();

```
