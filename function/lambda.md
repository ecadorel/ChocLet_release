# Lambda function

Lambda function are anonymous function, that is to say, it doesn't have a name.

```rust
let a = fn (a, b) => a + b;
// a is lambda function

assert (a (1, 2) == 3);
```

Lambda function are usefull for functional programming. They are mainly used in the standard library of choclet.
```rust
import std.algorithm.iteration;

let a = [1, 2, 3];
let b = a.map (fn (x) => x + 1);

assert (b == [2, 3, 4]);

// We can use array and lambda function for stream processing
let c = a.map (fn (x) => x + 1)
	     .filter (fn (x) => x <= 3)
		 .reduce (fn (a, b) => a + b);
		 
assert (c == 5 && a == [1, 2, 3]);
```
