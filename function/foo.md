# Function 

Function can be declared using the keyword `def`. The `return` statement can be used to return a value from the function.

```rust
def fibo (n) {
	if n < 2 return n;
    return fibo (n - 1) + fibo (n - 2);
}
// function that don't return value, actually return a void value
def foo (a, b) {
    println (a + b);
}
foo (1, 2);
foo ("salut", 1); // Error operator + undefined for types string and int 

def function (self, b) {
    println (self, b);
}
// a function can be called by the operator '.'
let a = [1, 2, 3];
a.function (10); // same as function (a, 10);
```
