# Globals 

```java
import std.choco.globals;
```

- **allDifferent** (*a*)-> ChocoConstraint |
  - Parameters : 
	- *a*, An array of ChocoInt
  - Returns : a ChocoConstraint (not posted) ensuring that all the value of *a* have a different value
  - Example : 
	```rust
	let a = [0 .. 10 | i in 0 .. 5];
	a.allDifferent ().post ();
	
	choco.solve ();
	println (a); // [0, 4, 2, 3, 1]
	```

- **allEqual** (*a*)-> ChocoConstraint | 
  - Parameters : 
	- *a*, An array of ChocoInt
  - Returns : a ChocoConstraint (not posted) ensuring that all the value of *a* have a different value
  - Example : 
	```rust
	let a = [0 .. 10 | i in 0 .. 5];
	a.allEqual ().post ();
	
	choco.solve ();
	println (a); // [0, 0, 0, 0, 0]
	```

