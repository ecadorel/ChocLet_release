# Packing

```java
import std.choco.packing;
```

- **simpleBinPacking** (*weights*, *capacities*)-> [ChocoInt]
  Post a binPacking constraints
  - Parameters : 
	- *weights*, An array of int encoding the weight of each element 
	- *capacities*, An array of ChocoInt encoding the load of each bin
  - Returns : an array of ChocoInt containing the position of each elements
	
  - Example :
	```rust
	let a = [4, 5, 3, 2];
	let b = [0 .. 10, 0 .. 7];
	
	let pos = simpleBinPacking (a, b);
	choco.solve ();
	println (pos); // [1, 0, 0, 0]
	println (b); // [10, 4]
	```
	
	
