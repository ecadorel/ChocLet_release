# Compute
	
```java
import std.choco.compute;
```

- **sum** (*a*)-> ChocoInt |
  Post a sum constraint on a ChocoInt array
  - Parameters :
	- *a*, a ChocoInt array
  - Returns: A ChocoInt, which will be equals to the sum of the array after solving
  - Example :
	```rust
	let a = [0 .. 2 | i in 0 .. 2];
	let b = a.sum ();
	
	choco.solve ();
	println (a, " ", b); // [1, 2] 3
	```

- **count** (*a*, *value*)-> ChocoInt |
  Post a count constraint on a ChocoInt array
  - Parameters : 
	- *a*, a ChocoInt array
	- *value*, an int value, or a ChocoInt
  - Returns: A ChocoInt that will be equals to the number of occurence of *value* in *a* after solving
  - Example :
	```rust
	let a = [0 .. 2 | i in 0 .. 2];
	let b = a.count (1);
	let c = 0 .. 2;
	let d = a.count (c);
  
    choco.solve ();
	println (a, " ", b, " "); // [1, 2] 1 
	println (c, " ", d); // 2 1
	```

- **among** (*vars*, *values*)-> ChocoInt |
  Post an among contraint
  - Parameters : 
	- *vars*, a ChocoInt array
	- *values*, an int array
  - Returns : the number of variables of the collection vars that take their value in values. 
  - Example :
	```rust
	let a = [0 .. 5 | i in 0 .. 2];
	let b = [1, 2];
	let c = a.among (b);
	
	choco.solve ();
	println (a, " ", c); // [1, 5] 1
	
	choco.solve ();
	println (a, " ", c); // [1, 2] 2
	
	choco.solve ();
	println (a, " ", c); // [1, 1] 2
	```

- **nbDiffValues** (*a*)-> ChocoInt |
  Post a nValues constraint.
  - Parameters : 
	- *a*, a ChocoInt array
  - Returns: A ChocoInt containing the number of different values there is in *a*
  - Example :
	```rust
	let a = [0 .. 2 | i in 0 .. 2];
	let b = a.nbDiffValues ();
	
	choco.solve ();
	println (a, " ", b); // [1, 2] 2
	
	choco.solve ();
	println (a, " ", b); // [1, 1] 1
	```

- **distance** (*a*, *b*)-> ChocoInt |
  Post a distance constraint
  - Parameters : 
	- *a*, a ChocoInt
	- *b*, a ChocoInt
  - Returns: A ChocoInt containing the distance between *a* and *b*
  - Example :
	```rust 
	let a = 0 .. 4, b = 0 .. 5;
	let c = a.distance (b);
	
	choco.solve ();
	println ("dist (", a, ", ", b, ") = ", c); // dist (3, 5) = 2
	```

