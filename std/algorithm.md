# Algorithm 

This group of module provides a set of classical algorithm.
It can be imported as follows : 
```java
import std.algorithm._;
```
	
This module is divided in sub modules :
- comparison
- iteration
- mutation
- searching
- sorting
