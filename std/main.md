# Standard modules

ChocLet provides a standard library with a collection of standard function.
Those modules are available every where under the global module name `std`.

Those modules ares : 
- [std.algorithm._](std/algorithm.md)
- [std.choco._](std/choco.md)
- [std.list](std/list.md)
- [std.math](std/math.md)
- [std.random](std/rand.md)



