# Set

The list module provides a set structure to store element, that guarantee there is only one occurence of each element in the set

```rust
import std.set;

let a = Set ([]);
a.add ([1, 2, 3]);

println (a.array); // [1, 2, 3]
a.add ([2, 3, 9]);

println (a.array); // [1, 2, 3, 9]

a.add ([0]);
println (a.array); // [1, 2, 3, 9, 0]
```

