# Iteration

```java
import std.algorithm.iteration;
```

- **map** (*array*, *func*) -> any |
  - Returns: an new array where each element at index *i* is equals to the result of *func* (*array* [i])
  - Parameters :
	- *array*, an array of element
	- *func*, an function *fn (x)-> y*, where *x* has the same type as an element of *array*
  - Example:
	```rust
	let a = ([1, 2, 3]).map (fn (x) => x + 1);
	assert (a == [2, 3, 4]);
	```
	
- **each** (*array*, *func*)-> void |
  Call the function *func* on each element of *array*
  - Parameters:
	- *array*, an array of element
	- *func*, a function *fn (x)-> void*, where *x* has the same type as an element of *array*
  - Example:
	```rust
	let a = [1, 2, 3];
	a.each (fn (x) => println (x)); // 1 2 3
	```

- **scan** (*array*, *func*)-> any |
  Perform a scan operation, *i.e* a cumulative reduction of *array*
  - Parameters:
	- *array*, an array of element
	- *func*, a function *fn (x, y)-> z*, where *x*, *y* and *z* have the same type as an element of *array*
  - Example:
	```rust
	let a = [1, 2, 3];
	let b = a.scan (fn (x, y) => x + y);
	assert (b == [1, 3, 6]); // [1, 1 + 2, 1 + 2 + 3]
	```
- **filter** (*array*, *func*)-> any |
  Filter an array of element using a predicate
  - Parameters :
	- *array*, an array of element
	- *func*, a function *fn (x)-> bool*, where *x* has the same type as an element of *array*
  - Example:
	```rust
	let a = [1, 2, 3, 4];
	let b = a.filter (fn (x) => x % 2 == 1);
	assert (b == [1, 3]);
	```
- **reduce** (*array*, *func*)-> any |
  Reducing an array to a single value
  - Parameters :
	- *array*, an array of element
	- *func*, a function *fn (x, y)-> z*, where *x*, *y* and *z* have the same type as an element of *array*
  - Example:
  ```rust
  let a = [1, 2, 3, 4];
  let b = a.reduce (fn (x, y) => a + b);
  assert (b == 10);
  ```
- **zip** (*a*, *b*, *func*)-> any
  Same as a map operation but use two array
  - Parameters:
	- *a*, an array of element
	- *b*, an array of element
	- *func*, a function *fn (x, y)-> z*, where *x* has the same type as an element of *a* and *y* has the same type as an element of *b*
  - Example:
  ```rust
  let a = [1, 2, 3];
  let b = ["one", "two", "three"];
  let c = zip (a, b, fn (x, y) => (x, y));
  assert (c == [(1, "one"), (2, "two"), (3, "three")])
  ```
