# Comparison 

```java
import std.algorithm.comparison;
```

- **min** (*a*, *b*) -> any |
  Return the minimal value between *a* and *b*, using the operator `<`
  - Returns: 
	  - *a* if *a* is lower than *b*, and *b* otherwise
- **max** (*a*, *b*) -> any |
  Return the maximal value between *a* and *b*, using the operator `>`
  - Returns: 
	  - *a* if *a* is greater than *b*, and *b* otherwise
- **clamp** (*val*, *lower*, *upper*) -> any |
  Clamp operation : => max (lower, min (upper, val))
  
