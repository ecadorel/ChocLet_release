# Sorting

```java
import std.algorithm.sorting;
```

- **sort** (*a*, *func*)-> any |
  Sort the array *a*, using the *func* operator as a element comparator
  - Parameters :
	- *a*, an array that will be sorted
	- *func*, a function *fn (x, y)-> bool* a comparator where *x* and *y* have the same type as an element of *a*
  - Example :
	```rust
	let a = [7, 4, 5, 9, 2];
	a.sort (fn (x, y) => a < b);
	assert (a == [2, 4, 5, 7, 9]);

	a.sort (fn (x, y) => a > b);
	assert (a == [9, 7, 5, 4, 2]);	
	```
