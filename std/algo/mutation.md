# Mutation

```java
import std.algorithm.mutation;
```

- **join** (*a*)-> any |
  Join an array of array
  - Parameters :
    - *a*, an array of array
  - Example:
	```rust
	let a = [[1, 2, 3], [3, 2, 1]];
	let b = a.join ();
	assert (b == [1, 2, 3, 3, 2, 1]);	
	```
- **reverse** (*a*)-> any |
  Reverse all the value of an array
  - Parameters :
   - *a*, an array
  - Example:
	```rust
	let a = [1, 2, 3];
	let b = a.reverse ();
	assert (b == [3, 2, 1]);
	```
