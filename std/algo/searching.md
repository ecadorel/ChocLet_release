# Searching

```java
import std.algorithm.searching;
```

- **all** (*array*, *func*)-> bool |
  Check a predicate on all the element of an array
  - Parameters:
	- *array*, an array
	- *func*, a function *fn (x)-> bool*, where *x* has the same type as an element of *array*
  - Example:
	```rust
	assert (([1, 2, 3]).all (fn (x) => x < 4));
	assert (!([3, 2, 8]).all (fn (x) => x % 2 == 0));
	```

- **any** (*array*, *func*)-> bool |
  Check if it exists an element in *array* that satisfy the predicate
  - Parameters : 
	- *array*, an array
	- *func*, a function *fn (x)-> bool*, where *x* has the same type as an element of *array*
  - Example:
	```rust
	assert (([1, 2, 3]).any (fn (x) => x < 3));
	assert (!([2, 4, 8]).any (fn (x) => x % 2 == 1));
	```

- **countUntil** (*a*, *b*)-> int |
  Count until *b* is found in *a*
  - Parameters :
	- *a*, an array 
	- *b*, an array, where *b* elements have the operator `!=` with the type of the elements of *a*
  - Example:
	```rust
	let a = "Hello World !!";
	assert (a.countUntil ("World") == 6);
	```
- **count** (*a*, *b*)-> int |
  Count the number of occurence of *b* element in *a*
  - Parameters :
	- *a*, an array
	- *b*, an element that possesses the operator `==` with *a* elements
  - Example :
	```rust
	let a = [1, 2, 2, 4, 2];
	assert (a.count (2) == 3);
	```

- **countPred** (*a*, *func*)-> int |
  Same as count but using a predicate
  - Parameters : 
	- *a*, an array
	- *b*, a function *fn (x)-> bool*, where *x* has the same type as an element of *a*
  - Example :
	```rust
	let a = [1, 3, 6, 9];
	assert (a.countPred (fn (x)=> x % 2 == 1) == 3);
	```
	
- **canFind** (*a*, *b*)-> bool |
  Returns : `true` if **countUntil** (*a*, *b*) != *a.len*
  
- **commonPrefix** (*a*, *b*)-> any |
  Returns: the common prefix of *a* and *b*
  - Parameters : 
	- *a*, an array
	- *b*, an array
  - Example :
	```rust
	assert (commonPrefix ("Hello World", "Hello Bob") == "Hello ");
	```

- **endsWith** (*a*, *b*)-> bool |
  Returns: `true` if *a* end with *b*
  - Parameters :
	- *a*, an array
	- *b*, an array
  - Example :
	```rust
	let a = "file.txt";
	assert (a.endsWith (".txt"));
	```

- **find** (*a*, *b*)-> int |
  Returns : the index of the element *b* in *a* or `-1`, if *b* is not in *a*
  - Parameters : 
	- *a*, an array
	- *b*, an element
  - Example :
	```rust
	assert (([1, 2, 3]).find (2) == 1);
	assert (([1, 2, 3]).find (7) == -1);
	```
	
- **findPred** (*a*, *func*)-> int |
  Returns : The index of the first element that satisfy the predicate
  - Parameters : 
	- *a*, an array
	- *func*, a function *fn (x)-> bool*, where *x* has the same type as an element of *a*
  - Example : 
  ```rust
  let a = [1, 2, 3];
  assert (a.findPred (fn (x)=> x % 2 == 0) == 1);
  ```

- **minElement** (*a*)-> any |
  Returns : the minimal element of the array *a*
  - Example :
	```rust 
	assert (([5, 2, 7, 9]).minElement () == 2);
	```
	
- **minIndex** (*a*)-> any |
  Returns : the index of the minimal element of the array *a*
  - Example :
	```rust 
	assert (([5, 2, 7, 9]).minElement () == 1);
	```
- **maxElement** (*a*)-> any |
  Returns : the maximal element of the array *a*
  - Example :
	```rust 
	assert (([5, 2, 7, 9]).minElement () == 9);
	```
	
- **minIndex** (*a*)-> any |
  Returns : the index of the maximal element of the array *a*
  - Example :
	```rust 
	assert (([5, 2, 7, 9]).minElement () == 3);
	```
