# Random

This modules provides some function to create random number

```java
import std.random;
```

- **uniform** (*lower*, *upper*)-> int |
  Return a number choosed randomly between *lower* and *upper* with uniform probability
  
- **choice** (*array*)-> any |
  Return an element of the array with uniform probability
  
- **shuffle** (*array*)-> any |
  Randomize the order of the array
  - Example : 
    ```rust
    let a = [1, 2, 3];
    a.shuffle ();
  
    println (a); // [2, 3, 1]
    ```
  
- **dice** (*proba*)-> int
  Return a number choosed randomly with different probability
  - Parameters : 
	- *proba*, an array of int assuming that the sum of the array is equal to 100
  - Example :
	```rust
	let a = [50, 20, 20, 10]; 
	a.dice (); // 50 % chance to get 0, 20 % to get 1 or 2 and 10 % to get 3
	```
