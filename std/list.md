# List

The list module provides a list structure to store element 

```rust
import std.list;

let lst = List ();
lst.push (10);
lst.printList (); // 10:>:NIL
println (lst.pop ()); // 10
lst.printList (); // Empty

for i in 0 .. 4
	lst.push (i);
	
lst.printList (); // 0:>:1:>:2:>:3:>:NIL
lst.remove (2); 
lst.printList (); // 0:>:1:>:3:>:NIL
```

### Methods 

- **push** (*list*, *value*)-> void |
  Push an element on the list
  
- **pop** (*list*)-> any | 
  Remove the last element of the list and return it
  
- **remove** (*list*, *value*)-> bool |
  Remove all the element equal to *value* in the *list* and return *true* if a removal was performed
	
- **removePred**(*list*, *func*)-> bool |
  Remove all the element in the *list* that satisfy the predicate *func*
  
- **printList** (*list*)-> void |
 Print the *list* to the screen
 
- **printReverse**(*list*)-> void |
  Print the list to the screen in reverse order
