# Control flow

ChocLet proposes multiple ways to control the execution flow : 
- If / Else construct
- While loop
- For loop
