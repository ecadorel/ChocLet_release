# While loop

The while keyword can be used to loop until a condition is unmet.

```rust
// A counter variable
let n = 1;
// Loop while n is less than 101
while n < 101 {
    if n % 2 == 0 {
        println ("even");
    } else {
        println ("odd");
    }
    n ++;
}
```

The while let construct can be used to iterate and store result of the condition until this result is `false`, `void` or `null`.

```rust
def foo (n) {
    if n < 100 return n + 1;
    else return;
}

// loop until n is void
let x = 10;
while let n = foo (x) {
    println (n);
    x++;
}
```
