# For loop

The `for` `in` construct can be used to iterate through an iterator. It can be used to iterate over a range type.
```rust
// Iterate from 0 (inclusive) to 101 (exclusive) [0; 101[
for i in 0 .. 101 { 
    if i % 2 == 0 {
        println ("even");
    } else {
        println ("odd");
    }
}
```
As for list comprehension, to include the second value of the range, we can use the operator `...`.

```rust
for i in 0 ... 4 {
	println (i); // 0 1 2 3 4
}

for i in 0 .. 4 {
	println (i); // 0 1 2 3
}
```

It can also iterate over arrays. 

```rust
let a = [1, 2, 3];    
for it in [3, 2, 1] {
    println (it); // 3, 2, 1
}
// The iterator is a reference 
for it in a {
    it = it + 1;
}
println (a); // [2, 3, 4]
```

