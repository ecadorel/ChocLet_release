# If / Else construct

If / Else is a construction used to branch conditionnaly.

```rust
let n = 5;
if n < 0 {
    println (n, " is negative");
} else if n > 0 {
    println (n, " is positive");
} else {
    println (n, " is zero");
}
```
