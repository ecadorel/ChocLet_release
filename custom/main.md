# Custom types

Structures are used to declare custom types. It's descriptor is created with the keyword struct

```rust
struct Point {
    new (self, x, y) {
        // In constructor context, all attribute exists
        self.x = x;
        self.y = y;
    }
}

struct Rectangle {
    new (self, x, y) {
        self.x = x;
        self.y = y;
    }
}

// all values are needed
let point = Point (.1, .7);
// point.z = 12;// Error, we are not in constructor context

let rect = Rectangle (point, Point (3.3, 4.5));

// Structure are printable.
println (rect);   
```
