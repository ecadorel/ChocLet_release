# List comprehension

List comprehension is a syntax to help to construct a list of element.

```rust
let a = [i | i in 0 .. 4]; 
assert (a == [0, 1, 2, 3]);

// a = [i | i in 0 .. 4] is the shorthand of :
let a = [];
for i in 0 .. 4 {
	a = a ~ [i];
}

// You can use if to filter some value
let b = [i | i in 0 .. 10 if i % 2 == 0];
assert (b == [0, 2, 4, 8]);

// An expression can be used for each index
let c = [i * 3 + 2 | i in 0 .. 4];
assert (c == [2, 5, 8, 11]);

// The operator ... can be used instead of .. to include the second value of the iterator
let d = [i | i in 0 ... 4];
assert (d == [0, 1, 2, 3, 4]);
```

