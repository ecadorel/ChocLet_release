# Literals 

The term literal refers to expressions that encode a value for a given type. 

The available literals are : 
- Integer literals : `2`, `782`, `1000`
- Floating point literals : `1.2`, `.3`, `4.`
- String literals : `'Hi !'`, `"How are you ?"`
- Boolean literals : `true`, `false`


Literals can be used as part of expression, and composed using
operators. Those operators and their precedences are the same as in
C-like languages.

```rust
// Floating point multiplication
println ("1.0 * 0.34 = ", 1. * .34);

// int substraction
println ("1 - 2 = ", 1 - 2);

// int logical expression 
println ("1 < 2 = ", 1 < 2);

// Boolean logic
println ("true and false is ", true && false);
println ("true or false is ", true || false);
println ("not true is ", !true);
```
