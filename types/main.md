# Basic types

ChocLet provides basic types to describe a program.
Those types are : 
 - `int` type desbribe a signed integer
 - `float` type describe a floating point type
 - `string` type encoded characters list
 - `bool` type which can be either true or false
 
 
A ChocLet program is dynamicly typed, and the types are always infered from expressions.

```rust
let a = 10; // a is an int 

// b is a bool, and c a string
let b = true, c = 'hi !'; 

a = false; // Error, we can't change the type of a variable
```
