# Tuple

Tuple are collection of value of different types. 

```rust
let a = (1, 'salut', 3.);
// a is a tuple of type (int, string, float)

println ("First element of a : ", a [0]);
println ("Len of a : ", a.arity);

for j in 0 .. a.arity {
	println (j);
}
```
