# Introduction

This book present the language **Choc**Let. ChocLet is a high level
interpreted programming language developed in Java. This language aims
to help developers to write [Choco](http://www.choco-solver.org/)
models with high expressivity. 

This book traverse all the programming concept of ChocLet, by showing
some example.

## Getting started

The first step is to install choclet. This part is easy, you will only need to download this jar file:
- [choclet.jar](https://gitlab.inria.fr/ecadorel/ChocLet_release/raw/master/release/choclet.jar)

And then launch it : 

```bash
$ java -jar choclet.jar
> println ("Hello World !")
Hello World !
```


