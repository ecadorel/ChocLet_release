# Modules

A module is a part of a program that can be imported from another part.

## Module creation 

A source file is a module, where its relative path is its module name.
The path is relative to the execution location of ChocLet.

```rust
// mods/test.clt
def foo () {
	println ("Foo");
}
```

```bash
$ tree
.
└── mods
    └── test.clt

$ java -jar choclet.jar
> import mods.test;
> foo ();
Foo
```

## Module execution

All the instruction in the global part of a module are not executed at importation.

```rust
// mods/test.clt
println ("ici");
```

```bash
> import mods.test;
> 
```

To execute the global part of a module you need to pass the file as an argument of the ChocLet jar.

```bash
$ java -jar choclet.jar mods/test.clt
$ ici
```

The option `-I` will give the hand on the interactive part just after the execution of the module.

```bash
$ java -jar choclet.jar -I mods/test.clt
$ ici
> 
```




